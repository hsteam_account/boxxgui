from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton
from PyQt5.QtGui import QKeySequence, QPalette, QColor
from PyQt5.QtCore import Qt, QTimer, QObject, QFile

import os, sys
import datetime
from hardware import Hardware
#from startWindow import StartWindow

import windows 

#Form, Window = uic.loadUiType("home.ui")

#app = QApplication([])


# Force the style to be the same on all OSs:
#app.setStyle("Fusion")

# Now use a palette to switch to dark colors:
#palette = QPalette()
#palette.setColor(QPalette.Window, QColor(53, 53, 53))
#palette.setColor(QPalette.WindowText, Qt.white)

#app.setPalette(palette)

#window = Window()

#form = Form()
#form.setupUi(window)
#window.show()
#window.showFullScreen()



#app.exec_()





class Home(QObject):
 
	def tick(self):
		#print('main tick')

		clockText = self.window.findChild(QLabel, 'clockLabel')

		now = datetime.datetime.now()
		timestr = now.strftime("%H:%M")
		clockText.setText(timestr)

	def __init__(self, parent=None):
		super(Home, self).__init__(parent)
		hardware=Hardware()
		hardware.reset()
		hardware.openDoor()
		hardware.SetLEDsBoxx(1,1,1)
		hardware.SetLEDsTop(0,0,1)

		ui_file = "home.ui"
	
		# self.line = self.window.findChild(QLineEdit, 'lineEdit')


		Form, Window = uic.loadUiType("home.ui")

		self.window = Window()

		self.form = Form()
		self.form.setupUi(self.window)

		self.timer = QTimer()
		self.timer.timeout.connect(self.tick)
		self.timer.start(1000)


		btn = self.window.findChild(QPushButton, 'startButton')
		btn.clicked.connect(self.start_handler)

		btn = self.window.findChild(QPushButton, 'introButton')
		btn.clicked.connect(self.intro_handler)

		self.show()

	def start_handler(self):
		print("pressed start")

		self.startWin = windows.ChooseWindow()
		self.startWin.show()
		self.window.close()

	def intro_handler(self):
		self.debugWin = windows.DebugWindow()
		self.timer.stop()
		self.window.close()
	
	def show(self):
		self.window.showFullScreen()

if __name__ == '__main__':
	app = QApplication(sys.argv)
	form = Home()
	sys.exit(app.exec_())
