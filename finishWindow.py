from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton
from PyQt5.QtGui import QKeySequence, QPalette, QColor, QPixmap
from PyQt5.QtCore import Qt, QTimer, QObject, QFile, QPropertyAnimation,QPoint, QThread

import os, sys
import datetime
import time, threading


import windows 
from pynfc import Nfc, Desfire, Timeout



def checkForConnection():
    for target in n.poll():
        try:
            print(target.uid, target.auth(DESFIRE_DEFAULT_KEY if type(target) == Desfire else MIFARE_BLANK_TOKEN))
            return
        except TimeoutException:
            pass

n = Nfc("pn532_i2c:/dev/i2c-1")

DESFIRE_DEFAULT_KEY = b'\x00' * 8
MIFARE_BLANK_TOKEN = b'\xFF' * 1024 * 4
def threadFunc(owner):
    checkForConnection()
    self.startWin = windows.Home()
    self.window.close() 

class WorkerThread(QThread):
    def run(self):
        checkForConnection()
class FinishWindow(QObject):

    def __init__(self, parent=None):
        super(FinishWindow, self).__init__(parent)
    
        # self.line = self.window.findChild(QLineEdit, 'lineEdit')


        Form, Window = uic.loadUiType("finish.ui")

        self.window = Window()

        self.form = Form()
        self.form.setupUi(self.window)


        self.notOkay=True
        okayButton=self.window.findChild(QPushButton, 'okayButton')
        #okayButton.clicked.connect(self.okay_handler)

        self.label=self.window.findChild(QLabel, 'countdownLabel')


    def show(self):

        self.window.setGeometry(-self.window.width(), 0, self.window.width(), self.window.height())
        #self.window.show()
        self.window.showFullScreen()

        #then a animation:
        animation = QPropertyAnimation(self.window, b"pos")
        animation.setDuration(10000)
        animation.setStartValue(self.window.pos())
        animation.setEndValue(QPoint(0,0))
         
        #to slide in call
        animation.start()




    def check(self):
        print("checking")
        self.x=WorkerThread()
        self.x.finished.connect(self.okay_handler)
        self.x.start()


    def okay_handler(self):
        if(self.notOkay):
            self.x.exit()
            self.notOkay=False
            self.label.setText("Unlock")
            #checkForConnection()
            self.startWin = windows.Home()
            self.window.close() 
        

