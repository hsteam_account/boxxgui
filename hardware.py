import adapter
from adapter import Adapter
from PyQt5.QtCore import Qt, QTimer, QObject, QFile, QPropertyAnimation,QPoint


import Adafruit_PCA9685
import time
from rpi_ws281x import *

import RPi.GPIO as GPIO


relay_heater = (0x58, 0)
relay_pump = (0x58, 1)	
hatch_exit_close = (0x58, 4)#former (0x58, 2)
hatch_exit_open = (0x58, 5)#former (0x58, 3)
hatch_bottle_close = (0x58, 6)#former (0x58, 4)
hatch_bottle_open = (0x58, 7)#former (0x58, 5)
hatch_misc_close = (0x58, 2)#former (0x58, 6)
hatch_misc_open = (0x58, 3)#former (0x58, 7)
relay_em_door = (0x58, 8)
relay_em_lock = (0x58, 9)
buzzer = (0x58, 10)
motor1_b = (0x58, 12)
motor1_a = (0x58, 13)
motor2_b = (0x58, 14)
motor2_a = (0x58, 15)

fan1 = (0x55, 0)
fan2 = (0x55, 1)
fan3 = (0x55, 2)
fan4 = (0x55, 3)
fan5 = (0x58, 11)

Valve2 = 27
Valve1 = 22
DOOR_LOCKED = 17



rgb1_r = (0x55, 4)
rgb1_g = (0x55, 5)
rgb1_b = (0x55, 6)
rgb2_r = (0x55, 7)
rgb2_g = (0x55, 8)
rgb2_b = (0x55, 9)
rgb3_r = (0x55, 10)
rgb3_g = (0x55, 11)
rgb3_b = (0x55, 12)
rgb4_r = (0x55, 13)
rgb4_g = (0x55, 14)
rgb4_b = (0x55, 15)

class Delay():
    def __init__(self, time, callback,owner,timeStep=1000,parent=None):
        self.time=time
        self.callback=callback
        self.owner=owner
        self.timer = QTimer()
        self.timer.timeout.connect(self.tick)
        self.timer.start(timeStep)
        

    def close(self):
        self.timer.stop()
        self.callback(self, self.owner)

    def tick(self):
        self.time-=1
        if self.time==0:
            self.callback(self,self.owner)
            self.owner.delays.remove(self)
            self.timer.stop()
        
class Hardware():
    def __init__(self, parent=None):
        self.adapters=(Adapter(),)
        self.delays=[]
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(Valve1, GPIO.OUT)
        GPIO.setup(Valve2, GPIO.OUT)
        GPIO.setup(DOOR_LOCKED, GPIO.IN)
        self.pwm58=Adafruit_PCA9685.PCA9685(address=0x58)
        self.pwm55=Adafruit_PCA9685.PCA9685(address=0x55)
    def __del__(self):
        for delay in self.delays:
            delay.close()
        GPIO.cleanup()
    def clear(self):
        for delay in self.delays:
            delay.close()
        self.delays=[]
    def reset(self):
        self.turnOffAllFans()
        self.deactivateOuterFans()
        self.deactivateOdorFan()
        self.deactivateShoeFan()
        self.deactivateClothingFan()
        self.deactivateMotherFan()
        self.turnOffAllUVs()
        self.turnOffDevice(relay_heater)
        self.turnOffDevice(relay_pump)
        self.deactivatePump()
        self.deactivateSteam()
       # self.openDoor()
    
    def turnOnUVs(self, index):
        self.adapters[index].turnOnUV1()
        self.adapters[index].turnOnUV2()
    def turnOffUVs(self, index):
        self.adapters[index].turnOffUV1()
        self.adapters[index].turnOffUV2()

    def turnOnDevice58(self,device):
        self.pwm58.set_pwm_freq(50)
        channel=device[1]
        self.pwm58.set_pwm(channel, 0, 4095)
    def turnOnDevice55(self,device):
        self.pwm55.set_pwm_freq(50)
        channel=device[1]
        self.pwm55.set_pwm(channel, 0, 4095)
    def turnOnDevice55To(self, device, amount):
        self.pwm55.set_pwm_freq(50)
        channel=device[1]
        self.pwm55.set_pwm(channel, 0, int(float(amount)*float(4095)))
    def turnOffDevice58(self,device):
        self.pwm58.set_pwm_freq(50)
        channel=device[1]
        self.pwm58.set_pwm(channel, 0, 0)
    def turnOffDevice55(self,device):
        self.pwm55.set_pwm_freq(50)
        channel=device[1]
        self.pwm55.set_pwm(channel, 0, 0)
    def turnOnDevice(self, device):
        if device[0]==0x58:
            self.turnOnDevice58(device)
        elif device[0]==0x55:
            self.turnOnDevice55(device)
    def turnOffDevice(self, device):
        if device[0]==0x58:
            self.turnOffDevice58(device)
        elif device[0]==0x55:
            self.turnOffDevice55(device)
    def turnOnAllUVs(self):
        self.turnOnUVs(0)
    def turnOffAllUVs(self):
        self.turnOffUVs(0)
    def turnOnAdapterFans(self, index):
        self.adapters[index].turnOnFans()

    def turnOffAdapterFans(self, index):
        self.adapters[index].turnOffFans()

    def turnOnAllFans(self):
        self.turnOnAdapterFans(0)

    def turnOffAllFans(self):
        self.turnOffAdapterFans(0)
        
    def toggleWithTimeStep(self, device, time, usedtimeStep):
        self.turnOnDevice(device)
        self.delays.append(Delay(time, lambda delay, lself: self.turnOffDevice(device), self, timeStep=usedtimeStep))

    def toggle(self, device, time):
        self.turnOnDevice(device)
        self.delays.append(Delay(time, lambda delay, lself: self.turnOffDevice(device), self))
        print(len(self.delays))

    
    
    def openHatchExit(self):
        print("open Exit")
        self.toggle(hatch_exit_open, 20)
    def closeHatchExit(self):
        print("close Exit")
        self.toggle(hatch_exit_close, 20)
    def openHatchBottle(self):
        self.toggle(hatch_bottle_open, 20)
    def closeHatchBottle(self):
        self.toggle(hatch_bottle_close, 20)
    def openHatchMisc(self):
        print("open Misc")
        self.toggle(hatch_misc_open, 20)
    def closeHatchMisc(self):
        print("close Misc")
        self.toggle(hatch_misc_close, 20)

    def openDoor(self):
        self.toggleWithTimeStep(relay_em_door, 1, 100)
    def activateDoorMagnet(self):
        self.turnOnDevice(relay_em_lock)

    def deactivateDoorMagnet(self):
        self.turnOffDevice(relay_em_lock)


    def activatePump(self):
        self.turnOnDevice(relay_pump)
    def deactivatePump(self):
        self.turnOffDevice(relay_pump)

    def activateHeat(self):
        print("activate Heat")

        self.turnOnDevice(relay_heater)
    def deactivateHeat(self):
        print("deactivate Heat")

        self.turnOffDevice(relay_heater)
    """
    def activateBottomFan(self):
        self.turnOnDevice(fan1)
    def deactivateBottomFan(self):
        self.turnOffDevice(fan1)
    def activateTopFan(self):
        print("activate Top Fan")
        self.turnOnDevice(fan2)

    def deactivateTopFan(self):
        print("deactivate Top Fan")
        self.turnOffDevice(fan2)

    def activateCaseFans(self):
        self.turnOnDevice(fan1)
        self.turnOnDevice(fan2)
    def deactivateCaseFans(self):
        self.turnOffDevice(fan1)
        self.turnOffDevice(fan2)
    """
    def activateOuterFans(self):
        self.turnOnDevice(fan1)
    def deactivateOuterFans(self):
        self.turnOffDevice(fan1)
    def activateOdorFan(self):
        self.turnOnDevice(fan2)
    def deactivateOdorFan(self):
        self.turnOffDevice(fan2)
    def activateClothingFan(self):
        self.turnOnDevice(fan3)
    def deactivateClothingFan(self):
        self.turnOffDevice(fan3)
    def activateShoeFan(self):
        self.turnOnDevice(fan4)
    def deactivateShoeFan(self):
        self.turnOffDevice(fan4)
    def activateMotherFan(self):
        self.turnOnDevice(fan5)
    def deactivateMotherFan(self):
        self.turnOffDevice(fan5)
    def activateSteam(self):
        for a in self.adapters:
            a.turnOnVapor()
    def deactivateSteam(self):
        for a in self.adapters:
            a.turnOffVapor()


    def doorLocked(self):
        pin=GPIO.input(DOOR_LOCKED)
        return True if pin==1 else False
    def buzz(self):
        self.toggle(buzzer, 1)

    def SetLED1(self,r,g,b):
        self.turnOnDevice55To(rgb1_r, r)    
        self.turnOnDevice55To(rgb1_g, g)    
        self.turnOnDevice55To(rgb1_b, b) 
    def SetLED2(self,r,g,b):
        self.turnOnDevice55To(rgb2_r, r)    
        self.turnOnDevice55To(rgb2_g, g)    
        self.turnOnDevice55To(rgb2_b, b)    
    def SetLED3(self,r,g,b):
        self.turnOnDevice55To(rgb3_r, r)    
        self.turnOnDevice55To(rgb3_g, g)    
        self.turnOnDevice55To(rgb3_b, b) 
    def SetLED4(self,r,g,b):
        self.turnOnDevice55To(rgb4_r, r)    
        self.turnOnDevice55To(rgb4_g, g)    
        self.turnOnDevice55To(rgb4_b, b)

    def SetLEDsBoxx(self, r,g,b):
        self.SetLED1(r,g,b)
        self.SetLED2(r,g,b)
    def SetLEDsTop(self, r,g,b):
        self.SetLED3(r,g,b)     
    def testAsync(self):
        print("first")
        self.testDelay=Delay(10, self.testCallback, self)
    def testCallback(delay, self):
        print("second")