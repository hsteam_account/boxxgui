from enum import Enum

class States(Enum):
    STARTING=1
    UV=2
    STEAM=3
    FIRSTDRY=4
    VAKUUM=5
    ODOR=6
    FINISH=7
class Step():
    def __init__(self, states, time):
        self.state=states
        self.time=time
class Data():
    def __init__(self):
        self.uvTime=10
        self.ironTime=0
        self.dryTime=10
        self.ventTime=1
        self.index=0
    def checkStepDone(self, progress):
        if self.currentTimeStep*60<progress:
            return True
        else:
            return False
    def getNextStep(self):
        if self.index>=len(self.steps):
            self.index-=1
        self.currentTimeStep=self.steps[self.index].time
        returnIndex=self.index
        self.index+=1
        return self.steps[returnIndex].state
    def calculateProgram(self):
        self.steps=[]
        if self.uvTime>0:
            self.steps.append(Step(States.UV, self.uvTime))
        if self.ironTime>0:
            self.steps.append(Step(States.STEAM, self.ironTime))
            self.steps.append(Step(States.FIRSTDRY, 1))
        if self.dryTime>0:
            self.steps.append(Step(States.VAKUUM, self.dryTime))
        self.steps.append(Step(States.ODOR,2/3))
        self.steps.append(Step(States.FINISH, self.ventTime))
        print(self.steps)