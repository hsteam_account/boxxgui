import smbus
import time

address = 0x18

VAPOR_CMD = 0x0C
PUMP_CMD = 0x0D
UV1_CMD = 0x0E
UV2_CMD = 0x0F

FAN1_CMD = 0x08
FAN2_CMD = 0x09
FAN3_CMD = 0x0A
FAN4_CMD = 0x0B

READADC_CMD_L = 0x06
READADC_CMD_H = 0x07
class Adapter():
    def __init__(self, parent=None):
         self.bus = smbus.SMBus(1)

    def turnOn(self, cmd):
        self.bus.write_byte_data(address, cmd, 1)
        data = self.bus.read_byte(address)

    def turnOff(self, cmd):
        self.bus.write_byte_data(address, cmd, 0)
        data = self.bus.read_byte(address)

    def turnOnVapor(self):
        self.turnOn(VAPOR_CMD)
    def turnOffVapor(self):
        self.turnOff(VAPOR_CMD)
    def turnOnUV1(self):
        self.turnOn(UV1_CMD)
    def turnOffUV1(self):
        self.turnOff(UV1_CMD)
    def turnOnUV2(self):
        self.turnOn(UV2_CMD)
    def turnOffUV2(self):
        self.turnOff(UV2_CMD)
    def turnOnFans(self):
        self.turnOn(FAN1_CMD)
        self.turnOn(FAN2_CMD)
        self.turnOn(FAN3_CMD)
        self.turnOn(FAN4_CMD)
    def turnOffFans(self):
        self.turnOff(FAN1_CMD)
        self.turnOff(FAN2_CMD)
        self.turnOff(FAN3_CMD)
        self.turnOff(FAN4_CMD)

    def triggerSwitch(self,cmd):
        self.bus.write_byte_data(address, cmd, 1)
        data = self.bus.read_byte(address)
        print(data)
        time.sleep(6)
        self.bus.write_byte_data(address, cmd, 0)
        data = self.bus.read_byte(address)
        print(data)

    def getTemperature(self):
        self.bus.write_byte_data(address, READADC_CMD_L, 1)
        data1 = self.bus.read_byte(address)
        self.bus.write_byte_data(address, READADC_CMD_H, 1)
        data2 = self.bus.read_byte(address)
        val = (data1 + (data2<<8))/4095.0*3.3*33.33333
        print("Temp: ", val)

    def getCurrent(self):
        self.bus.write_byte_data(address, READADC_CMD_L, 0)
        data1 = self.bus.read_byte(address)
        self.bus.write_byte_data(address, READADC_CMD_H, 0)
        data2 = self.bus.read_byte(address)
        offset = 0.044
        zeropos = 1.65
        apervolt = 15.15151515
        val = ((data1 + (data2<<8))/4095.0*3.3+offset-zeropos)*apervolt
        print("Current: ", val)

    