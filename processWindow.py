from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton
from PyQt5.QtGui import QKeySequence, QPalette, QColor, QPixmap
from PyQt5.QtCore import Qt, QTimer, QObject, QFile, QPropertyAnimation,QPoint
from programData import Data, States

#import main
import os, sys
import datetime
import time

import windows
from enum import Enum
from hardware import Hardware

class ProcessWindow(QObject):

    """
    def closeHatches(self):
        if(closeTime==60):
            self.hardware.closeHatchMisc()
        elif(closeTime)
        self.closeTime-=1
    """
    def tick(self):
        #print('process tick')
        countdownText = self.window.findChild(QLabel, 'countdownLabel')
        processText = self.window.findChild(QLabel, 'processLabel')
        if self.state!=States.STARTING:
            self.startTs -= 1
            self.progress+=1
        min = int(self.startTs/60)
        sec = int(self.startTs - min*60)
        progressMins=(self.progress/60)
        #timestr = now.strftime("%H:%M")
        countdownText.setText("{:02d}:{:02d}".format(min, sec))
        if self.state==States.STARTING:
            if True| self.hardware.doorLocked():
                #Tür geschlossen, programm wird gestarted mit UV zuerst
                self.switchState(self.data.getNextStep(),processText)
            else:
                processText.setText("Please Close the door")
        elif self.data.checkStepDone(self.progress):
            self.switchState(self.data.getNextStep(),processText)
        if self.startTs == 0:
            #Zeit ist um 
            #alles aus
            self.hardware.buzz()

            self.hardware.SetLEDsBoxx(0,1,0)
            self.cancel_handler()

    def __init__(self, parent=None):
        super(ProcessWindow, self).__init__(parent)
    
        # self.line = self.window.findChild(QLineEdit, 'lineEdit')


        Form, Window = uic.loadUiType("process.ui")

        self.window = Window()

        self.form = Form()
        self.form.setupUi(self.window)
        self.hardware=Hardware()
        btn = self.window.findChild(QPushButton, 'cancelButton')
        btn.clicked.connect(self.cancel_handler)

    def cancel_handler(self):
        print("pressed cancel")
        self.hardware.clear()
        self.hardware.reset()
        self.timer.stop()
        self.finishWin = windows.FinishWindow()
        self.finishWin.show()
        self.window.close()
        self.finishWin.check()
        
    def switchState(self, state, processText):
            self.hardware.clear()
            self.hardware.buzz()
            self.state=state
            self.progress=0
            print("switch To State",state)
            if state==States.UV:
                self.toUVState(processText)
            elif state==States.STEAM:
                self.toIronState(processText)
            elif state==States.FIRSTDRY:
                self.toFirstAirOut(processText)
            elif state==States.VAKUUM:
                self.toVakuum(processText)
            elif state==States.ODOR:
                self.toOdor(processText)
            elif state==States.FINISH:
                self.toFinish(processText)
    def toUVState(self,processText):
        processText.setText("Irradiation...")
        self.hardware.closeHatchExit()
        self.hardware.closeHatchMisc()
        self.hardware.closeHatchBottle()

        self.hardware.turnOnAllUVs()

        self.hardware.turnOnAllFans()
        self.hardware.deactivateOuterFans()
        self.hardware.activateShoeFan()
        self.hardware.activateClothingFan()
        self.hardware.activateMotherFan()
        self.hardware.deactivateOdorFan()

        self.hardware.deactivateSteam()

        self.hardware.SetLEDsBoxx(0,0,1)
        self.state=States.UV

    def toIronState(self, processText):
        processText.setText("Ironing...")
        self.hardware.closeHatchExit()
        self.hardware.closeHatchMisc()
        self.hardware.closeHatchBottle()

        self.hardware.turnOffAllUVs()

        self.hardware.turnOnAllFans()
        self.hardware.deactivateOuterFans()
        self.hardware.activateShoeFan()
        self.hardware.activateClothingFan()
        self.hardware.activateMotherFan()
        self.hardware.deactivateOdorFan()

        self.hardware.activateSteam()


        self.hardware.SetLEDsBoxx(1,0,0)
        self.state=States.STEAM
    def toFirstAirOut(self, processText):
        processText.setText("Ventilation...")
        self.hardware.openHatchExit()
        self.hardware.openHatchMisc()
        self.hardware.closeHatchBottle()

        self.hardware.turnOffAllUVs()

        self.hardware.turnOnAllFans()
        self.hardware.activateOuterFans()
        self.hardware.activateShoeFan()
        self.hardware.activateClothingFan()
        self.hardware.activateMotherFan()
        self.hardware.deactivateOdorFan()

        self.hardware.deactivateSteam()


        self.hardware.SetLEDsBoxx(1,1,1)
        self.state=States.FIRSTDRY
    def toVakuum(self, processText):
        processText.setText("Vacuum...")
        self.hardware.closeHatchExit()
        self.hardware.closeHatchMisc()
        self.hardware.closeHatchBottle()

        self.hardware.turnOffAllUVs()

        self.hardware.turnOnAllFans()
        self.hardware.deactivateOuterFans()
        self.hardware.activateShoeFan()
        self.hardware.activateClothingFan()
        self.hardware.activateMotherFan()
        self.hardware.deactivateOdorFan()

        self.hardware.deactivateSteam()

        self.hardware.SetLEDsBoxx(0,0,1)
        self.state=States.VAKUUM
    def toOdor(self, processText):
        processText.setText("Scent...")
        self.hardware.closeHatchExit()
        self.hardware.closeHatchMisc()
        self.hardware.openHatchBottle()

        self.hardware.turnOffAllUVs()

        self.hardware.turnOnAllFans()
        self.hardware.deactivateOuterFans()
        self.hardware.activateShoeFan()
        self.hardware.activateClothingFan()
        self.hardware.activateMotherFan()
        self.hardware.activateOdorFan()

        self.hardware.deactivateSteam()

        self.hardware.SetLEDsBoxx(0,0,1)
        self.state=States.ODOR
    def toFinish(self, processText):
        processText.setText("Ventilation...")
        self.hardware.openHatchExit()
        self.hardware.openHatchMisc()
        self.hardware.closeHatchBottle()

        self.hardware.turnOffAllUVs()

        self.hardware.turnOnAllFans()
        self.hardware.activateOuterFans()
        self.hardware.activateShoeFan()
        self.hardware.activateClothingFan()
        self.hardware.activateMotherFan()
        self.hardware.deactivateOdorFan()

        self.hardware.deactivateSteam()


        self.hardware.SetLEDsBoxx(1,1,1)
        self.state=States.FINISH
    def start_Timer(self, data):
        self.timer = QTimer()
        self.timer.timeout.connect(self.tick)
        self.timer.start(1000)
        self.state=States.STARTING


        self.data=data
        self.startTs=60*data.fullTime
        self.progress=0
        self.fullTime=self.startTs

    def show(self):

        self.window.setGeometry(-self.window.width(), 0, self.window.width(), self.window.height())
        #self.window.show()
        self.window.showFullScreen()

        #then a animation:
        animation = QPropertyAnimation(self.window, b"pos")
        animation.setDuration(10000)
        animation.setStartValue(self.window.pos())
        animation.setEndValue(QPoint(0,0))
         
        #to slide in call
        animation.start()

    

    

    def start_handler(self):
        print("pressed start")

        #self.startWin = StartWindow()
        #self.startWin.show()

